require(["./config"], function() {
    require([
        'bootstrapModal',
        // 'bootstrapCarousel',
        // 'partials/modal-gallery',
        'partials/modal-slider',
        'partials/modal-vertical-align',
        'partials/show-tooltip',
        'partials/scroll-block'
    ]);
});