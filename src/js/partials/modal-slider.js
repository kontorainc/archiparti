//- ====================
//- archiparti: modal-gallery
//- 04-10-2016: drtvader
//- --------------------
//- Отключение автопрокрутки карусели в модальном окне
//- ====================
define(['jquery', 'slick-carousel'], function($) {
    $('.js-slick-slider').slick({
        dots: false,
        arrows: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        initialSlide: 0,
        prevArrow: '<a href="javascript:void();" class="b-modal-gallery__carousel-control carousel-control left"><i class="icon-back"></i></a>',
        nextArrow: '<a href="javascript:void();" class="b-modal-gallery__carousel-control carousel-control right"><i class="icon-send"></i></a>'
    });


    $('#gallery').on('show.bs.modal', function (e) {
        $('.js-slick-slider').slick('refresh');
    })
});