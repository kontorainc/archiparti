//====================
// TSV: b-project-list-scroll
// 01-11-2016: Malich
//---------------------
// При скроле меняем классы
//====================
define(['jquery'], function($) {
    $('.js-project-list-scroll').on('scroll', function() {
        var count = 0;
        var elements = $(this).find('.b-project-list__item');
        var elemHeight = elements.first().height();
        var lastElem = Math.floor(($(this).scrollTop()) / elemHeight);

        elements.removeClass('b-project-list__item--visible b-project-list__item--transparent');
        elements.each(function(i, val) {
            if (i >= lastElem) {
                if (count < 3) {
                    $(this).addClass('b-project-list__item--visible');
                    count++;
                } else if (count == 3) {
                    $(this).addClass('b-project-list__item--transparent');
                    count++;
                }
            }
        });
    });
});