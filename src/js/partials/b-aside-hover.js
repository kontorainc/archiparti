//====================
// TSV: b-aside-hover
// 01-11-2016: Malich
//---------------------
// При долгом навидении ховера открываем менюшку и скрываем если убрали
//====================
define(['jquery'], function($) {
	var clearTimeAsideHover;
	var opensHover = false;
	var maxOMin = false;
	var self;

	if ($('.js-aside-hover').parents('.b-aside').hasClass('b-aside--min-menu'))
		maxOMin = true;
	else
		maxOMin = false;

	//Ховер элемента items из project
	$('.js-aside-hover').hover(
		//Приход на элемент
		function() {
			if (!opensHover) {
				clearTimeout(clearTimeAsideHover);
				self = $(this);
				clearTimeAsideHover = setTimeout(function() {
					if (maxOMin) {
						self.parents('.b-aside').removeClass('b-aside--min-menu').addClass('b-aside--max-menu');
						opensHover = true;
					} else {
						self.parents('.b-aside').removeClass('b-aside--studio-menu').promise().done(function() {
							self.parents('.b-aside').addClass('b-aside--min-menu').promise().done(function() {
								setTimeout(function() {
									self.parents('.b-aside').removeClass('b-aside--min-menu').addClass('b-aside--max-menu');
									opensHover = true;
								}, 500);
							});
						});
					}
				}, 675);
			}
		},
		function() {
			clearTimeout(clearTimeAsideHover);
			if ($(this).hasClass('js-aside-hover b-project-list__item')) {
				opensHover = false;
			}
		}
	);

	$('.b-project-menu--project').on({
		mouseleave: function(e) {
			if (opensHover) {
				clearTimeout(clearTimeAsideHover);
				if (maxOMin) {
					self.parents('.b-aside').removeClass('b-aside--max-menu').addClass('b-aside--min-menu');
					opensHover = false;
				} else {
					self.parents('.b-aside').removeClass('b-aside--max-menu').promise().done(function() {
						self.parents('.b-aside').addClass('b-aside--min-menu').promise().done(function() {
							setTimeout(function() {
								self.parents('.b-aside').removeClass('b-aside--min-menu').addClass('b-aside--studio-menu');
								opensHover = false;
							}, 500);
						});
					});
				}
			}
		}
	});
});