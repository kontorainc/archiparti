//====================
// TSV: task-chat-form
// 14-11-2016: Malich
//---------------------
// Считование события hover и click, для изменения scrollTop
//====================
define(['jquery'], function($) {

    var taskFormHeightDef = $('.js-task-chat-on').find('.b-form').height();
    var taskChatPaddingBottom = parseInt($('.js-task-chat-on').find('.js-scroll-block').css('paddingBottom').replace('px', ''));
    var taskFormHeightNew;

    $('.js-task-chat-on').find('.b-form__group').on({
        mouseenter: function() {
            $(this).delay(66).promise().done(function() {
                taskFormHeightNew = Math.round($('.js-task-chat-on').find('.b-form').height());
                taskChatPaddingBottom += (taskFormHeightNew - taskFormHeightDef);
                $('.js-task-chat-on').find('.js-scroll-block').animate({
                    scrollTop: parseInt($('.js-task-chat-on').find('.js-scroll-block').scrollTop() + taskChatPaddingBottom)
                }, 450);
            });
        },
        click: function() {
           $(this).delay(66).promise().done(function() {
                taskFormHeightNew = Math.round($('.js-task-chat-on').find('.b-form').height());
                taskChatPaddingBottom += (taskFormHeightNew - taskFormHeightDef);
                $('.js-task-chat-on').find('.js-scroll-block').animate({
                    scrollTop: parseInt($('.js-task-chat-on').find('.js-scroll-block').scrollTop() + taskChatPaddingBottom)
                }, 450);
            });
        }
    });
});