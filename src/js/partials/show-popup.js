//====================
// TSV: show-popup
// 25-10-2016: Malich
//---------------------
// Плавоное окрытие и закрытие попапа
//====================
define(['jquery', '../variables', 'bootstrapModal'], function($, globalVars) {

    var cloneElementShadow;
    $('.popup').on('shown.bs.modal', function(e) {
        var self = $(this);
        self.css({display: 'none'}).promise().done(function() {
            $('.js-clone-element').remove().promise().done(function() {
                cloneElementShadow = $('.modal-backdrop').clone().addClass('js-clone-element').css({display: 'none'});
                $('body').append(cloneElementShadow);
                $('.modal-backdrop').first().remove();
            }).promise().done(function() {
                self.delay(150).fadeIn(globalVars.animationTime);
                cloneElementShadow.fadeIn(globalVars.animationTime);
            });
        });
    });

    $('.popup').on('hidden.bs.modal', function() {
        var self = $(this);
        self.css({display: 'block'}).promise().done(function() {
            self.delay(150).fadeOut(globalVars.animationTime);
            $('.modal-backdrop').fadeOut(globalVars.animationTime);
        });
    });
});