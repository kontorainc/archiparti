//====================
// TSV: preloader
// 14-11-2016: Malich
//---------------------
// Прогрузка страницы
//====================
define(['jquery'], function($) {
    var preloader = $('.preloader');
    $(window).on('load').promise().done(function() {
        preloader.delay(2000).fadeOut(500);
    });
});
