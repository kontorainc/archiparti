//====================
// TSV: task-chat
// 14-11-2016: Malich
//---------------------
// При открытии чата скролим чат до конца
//====================
define(['jquery'], function($) {
    var taskChatBlockHeight = $('.js-task-chat-on').find('.js-scroll-block');
    var taskChatFormHeight = $('.js-task-chat-on').find('.b-form');

    var topForScroll = (taskChatFormHeight.height() + taskChatBlockHeight.outerHeight()) + parseInt(taskChatBlockHeight.css('paddingTop').replace('px', ''));
    taskChatBlockHeight.scrollTop(topForScroll);

    $('.js-task-chat-on').find('.js-scroll-block').on('scroll', function() {
        if ($('.js-task-chat-on').hasClass('js-task-chat-on')
            && $('.js-task-chat-off').hasClass('js-task-chat-off')) {

            if ($('.js-info-task-icon').hasClass('active')) {
                $('.b-info-task').slideUp(500).promise().done(function() {
                   $('.js-info-task-icon').removeClass('active').promise().done(function() {
                        $('.js-info-task-cont').removeClass('active').promise().done(function() {
                            $('.b-info-task').slideDown(500);
                        });
                    });
                });
            }
        }
    });
});