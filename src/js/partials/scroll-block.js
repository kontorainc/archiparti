//- ====================
//- archiparti: scroll-block
//- 17-10-2016: Mel. Blimm
//- --------------------
//- Скролл любого блока по вертикали
//- ====================
define(['jquery', 'perfect-scrollbar.jquery'], function($) {
    $(".js-scroll-block").perfectScrollbar({
        // suppressScrollX: true
    });
});