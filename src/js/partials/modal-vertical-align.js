/*
 * Проект: rads
 * 24-10-2016: sugar
 * ---------------------
 * nav
 */
define(['jquery'], function ($) {
    $(document).ready(function() {
    	$('.modal-vertical-align').on('show.bs.modal', function(e) {
    		centerModals($(this));
    	});
    });


    function centerModals($element) {
        var $modals;
        if ($element.length) {
            $modals = $element;
        } else {
            $modals = $(modalVerticalCenterClass + ':visible');
        }
        $modals.each( function(i) {
            var $clone = $(this).clone().css('display', 'block').appendTo('body');
            var top = Math.round(($clone.height() - $clone.find('.b-modal').height()) / 2);
            top = top > 0 ? top : 0;
            $clone.remove();
            $(this).find('.b-modal').css("margin-top", top);
        });
    }

});