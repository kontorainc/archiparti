//==============================
// Archiparti: show-project-list
// 06-10-2016: drtvader
//---------------------
// Появление списка списка проектов в шапке
//=======================================
define(['jquery'], function ($) {
    $('.js-show-menu').hover(function() {
        $(this).next().show(); // получаем следующий за данным .link элемент (т.е. span)
    },
    function() {
        $(this).next().hide();
    });
});
