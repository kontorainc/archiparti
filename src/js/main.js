require(['./config',], function() {
    require([
        'jquery',
        'partials/scroll-block',
        'partials/b-project-list-scroll',
        'partials/tooltip-arrow-left'
    ]);
});