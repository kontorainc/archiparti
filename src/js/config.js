require.config({
    baseUrl: 'js',
    paths: {
        jquery: 'jquery/jquery',
        header: 'partials/base/header',
        footer: 'partials/base/footer',
        html5shiv: 'bower_components/html5shiv/dist/html5shiv',
        'jquery-placeholder': 'bower_components/jquery-placeholder/jquery.placeholder',
        'jquery-ui': 'bower_components/jquery-ui/jquery-ui',
        modernizr: 'bower_components/modernizr/modernizr',
        bootstrap: 'bower_components/bootstrap/dist/js/bootstrap',
        bootstrapModal: 'bower_components/bootstrap/js/modal',
        'jquery-validation': 'bower_components/jquery-validation/dist/jquery.validate',
        'jquery.maskedinput': 'bower_components/jquery.maskedinput/dist/jquery.maskedinput',
        select2: 'bower_components/select2/dist/js/select2',
        'jquery-touchswipe': 'bower_components/jquery-touchswipe/jquery.touchSwipe',
        'perfect-scrollbar.jquery': 'bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery',
        'perfect-scrollbar': 'bower_components/perfect-scrollbar/js/perfect-scrollbar',
        'slick-carousel': 'bower_components/slick-carousel/slick/slick',
        'jquery.fullpage': 'bower_components/fullpage.js/dist/jquery.fullpage',
        'jquery.easings': 'bower_components/fullpage.js/vendors/jquery.easings.min',
        scrolloverflow: 'bower_components/fullpage.js/vendors/scrolloverflow.min'
    },
    packages: [

    ],
    shim: {
        'slick-carousel': [
            'jquery'
        ],
        bootstrapModal: [
            'jquery'
        ],
        bootstrapCollapse: [
            'jquery'
        ],
        bootstrapTransition: [
            'jquery'
        ],
        bootstrapTooltip: [
            'jquery'
        ],
        bootstrapPopover: [
            'jquery',
            'bootstrapTooltip'
        ],
        bootstrapTab: [
            'jquery'
        ],
        'jquery-ui': [
            'jquery'
        ],
        'jquery.maskedinput': [
            'jquery'
        ],
        'bootstrap-carousel-swipe': [
            'jquery'
        ],
        'jquery-shadow-animation': [
            'jquery'
        ]
    }
});

//- Модули, подключаемые на всех страницах
require([
    'modernizr',
    'header',
    'partials/rubber-band'
]);