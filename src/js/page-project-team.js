require(['./config',], function() {
    require([
        'partials/scroll-block',
        'bootstrapModal',
        'partials/modal-slider',
        'partials/modal-vertical-align',
        'partials/tooltip-status-change',
        'partials/tooltip-arrow-left',
        'partials/info-task-icon',
        'partials/task-chat',
        'partials/task-chat-form',
        'partials/all-files-show',
        'partials/b-aside-hover',
        'partials/b-project-list-scroll',
        'partials/show-popup',
        'slick-carousel'
        // 'partials/preloader'
    ]);
});