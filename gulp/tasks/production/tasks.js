'use strict';

const gulp = require('gulp');

function lazyRequireTask(taskName, path, options) {
    options = options || {};
    options.taskName = taskName;
    gulp.task(taskName, function(callback) {
        let task = require(path).call(this, options);

        return task(callback);
    });
}

lazyRequireTask('iconfont', './tasks/iconfont');

// Sprites
lazyRequireTask('sprites-png', './tasks/sprites-png');
lazyRequireTask('sprites-svg', './tasks/sprites-svg');
gulp.task('sprites', gulp.parallel('sprites-png', 'sprites-svg'));


lazyRequireTask('images', './tasks/images');

lazyRequireTask('jade', './tasks/jade');

lazyRequireTask('ieless', './tasks/ieless');

lazyRequireTask('bower-requirejs', './tasks/bower-requirejs');

lazyRequireTask('js', './tasks/js');

lazyRequireTask('json', './tasks/json');

lazyRequireTask('fonts', './tasks/fonts');

// Less

// Concate LESS for Desktop and Mobile
lazyRequireTask('concat-mobile', './tasks/less-concate', {
    lessName:     '*/*.mobile.less',
    concatName:   'bem.mobile.less'
});

lazyRequireTask('concat-tablet', './tasks/less-concate', {
    lessName:     '*/*.tablet.less',
    concatName:   'bem.tablet.less'
});

lazyRequireTask('concat-desktop-min', './tasks/less-concate', {
    lessName:     '*/*.desktop.min.less',
    concatName:   'bem.desktop.min.less'
});

lazyRequireTask('concat-desktop', './tasks/less-concate', {
    lessName:     '*/*.desktop.less',
    concatName:   'bem.desktop.less'
});

lazyRequireTask('concat-desktop-big', './tasks/less-concate', {
    lessName:     '*/*.desktop.big.less',
    concatName:   'bem.desktop.big.less'
});

// Less compile
lazyRequireTask('less-compile', './tasks/less-compile');

// Compile LESS to CSS
gulp.task('less', gulp.series(
    gulp.parallel(
        'concat-mobile',
        'concat-tablet',
        'concat-desktop-min',
        'concat-desktop',
        'concat-desktop-big'
    ),
    'less-compile'
));

lazyRequireTask('clean', './tasks/clean');

lazyRequireTask('htmllist', './tasks/htmllist');

gulp.task('compile',
    gulp.series(
        gulp.parallel(
            'iconfont',
            'sprites',
            'images',
            'jade',
            'ieless',
            'js',
            'json',
            'bower-requirejs'
        ),
        'fonts',
        'less'
    )
);

//TODO: add testSystem task
gulp.task('init', gulp.series('htmllist'));

gulp.task('build',
    gulp.series(
        'clean',
        gulp.parallel(
            'compile',
            'init'
        )
    )
);