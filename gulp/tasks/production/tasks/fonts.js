'use strict';

const $      = require('gulp-load-plugins')();
const gulp   = require('gulp');
const config = require('../../../config');

/*
 * Copy fonts
 */
module.exports = function(options) {
    return function() {
        return gulp.src(config.fonts.src)
            .pipe(gulp.dest(config.fonts.dest));
    }
};