var build = 'build/',
    base  = 'src/';

module.exports = {
    build: build,
    base: base,
    other: {
        src: [base + '*.*', '!' + base + '*.jade'],
    },
    node: {
        src: 'node_modules/',
        dest: build + 'node_modules'
    },
    fonts: {
        src: base + 'fonts/**/*.*',
        dest: build + 'fonts/',
    },
    iconfont: {
        src: base + 'images/svg_for_icon/*.svg',
        dest: build + 'fonts/',
        fontName: 'icons'
    },
    jade: {
        src: base + '*.jade',
        dest: build,
        expand: true
    },
    js: {
        srcJquery: 'src/js/bower_components/jquery/dist/*.*',
        destJquery: build + 'js/jquery/',
        srcHtml5shiv: 'src/js/bower_components/html5shiv/dist/*.*',
        destHtml5shiv: build + 'js/html5shiv/',
        srcNormalize: 'src/js/bower_components/normalize-css/normalize.css',
        destNormalize: build + 'css/normalize/',
        src: base + 'js/**/*.js',
        dest: build + 'js/',
        requireJs: true
    },
    json: {
        src: base + 'json/**/*.*',
        dest: build + 'json/',
    },
    sprites: {
        src: base + 'images/sprite/**/*.*',
        src2: base + 'images/sprite@2x/**/*.*',
        srcSvg: base + 'images/spritesvg/*.svg',
        options: {
            imgName: 'sprite.png',
            cssName: 'sprite.less',
            imgPath: '../images/sprite.png',
            cssFormat: 'less',
            algorithm: 'top-down',
            padding: 10,
            engine: 'pngsmith',
            imgOpts: {
                format: 'png'
            }
        },
        options2: {
            imgName: 'sprite@2x.png',
            cssName: 'sprite2.less',
            imgPath: '../images/sprite2.png',
            cssFormat: 'less',
            algorithm: 'top-down',
            padding: 10,
            engine: 'pngsmith',
            imgOpts: {
                format: 'png'
            }
        },
        optionsSvg: {
            imgPath: '../spritesvg.svg',
            cssPath: '../../../src/style/partials/spritepng.less'
        },
        dest: {
            css: base + 'style/partials/',
            image: base + 'images/'
        },
    },
    images: {
        src: [
            base + 'images/**/*.*',
            '!' + base + 'images/sprite/**/*.*',
            '!' + base + 'images/sprite@2x/**/*.*',
            '!' + base + 'images/svg_for_icon/*.svg',
            '!' + base + 'images/spritesvg/**/*.*'
        ],
        dest: build + 'images/',
        imagemin: {
            // use добавляется в таске
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            interlaced: true
        }
    },
    less: {
        concatless: base + 'style/concat-less/',
        bemblocks: base + 'style/bem-blocks/',
        src: base + 'style/main.less',
        dest: build + 'css/'
    },
    clean: {
        src: './' + build
    },
    ieless: {
        src: base + 'css/*.less',
        dest: build + 'css/'
    },
    autoprefixer: {
        browsers: [
            'last 10 versions',
            'ie 8',
            'ie 9'
        ]
    },
    bower: 'src/js/bower_components/**/*.*',
    wrapPipe: function(taskFn) {
        return function(done) {
            var onSuccess = function() {
                done();
            };
            var onError = function(err) {
                done(err);
            }
            var outStream = taskFn(onSuccess, onError);
            if(outStream && typeof outStream.on === 'function') {
                outStream.on('end', onSuccess);
            }
        }
    }
};